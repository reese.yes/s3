// console.log('hello')

//By convention, class names shd begin w/ an uppercase char
class Student {   // declaring a class;blueprint, plan, guidelines;no props
	// the constructor method defines how obj created fr this class will be assigned their initial prop values
	constructor(name, email, grades){
		this.name = name   // "this" refers to obj we create fr the class, not the class itself
		this.email = email
		this.gradeAve = undefined
		this.studentPassed = undefined
		this.passedWithHonors = undefined

		// for 'grades'; array of 4 
		if(grades.length === 4) {
			if(grades.every(grade => grade >= 0 && grade <= 100)) {
				this.grades = grades
			}else {
				this.grades = undefined
			}
		}else {
			this.grades = undefined
		}
	}

	//methods must be outside of constructor
	login(){
		console.log(`${this.email} has logged in.`)
		return this
	}
	
	logout(){
		console.log(`${this.email} has logged out.`)
		return this
	}
	
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}

	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		// return sum/4
		this.gradeAve = sum/4;
		return this  // --- double return not allowed
	}

	willPass(){
		// return this.computeAve() >= 85 ? true : false;
		this.studentPassed = this.computeAve().gradeAve >= 85 ? true : false;
		return this
	}

	willPassWithHonors(){
		if(this.computeAve().gradeAve >= 90){
			//return true
			this.studentWithHonors = true
		}else if(this.computeAve().gradeAve < 90 && this.computeAve().gradeAve >= 85){
			//return false
			this.studentWithHonors = false
		}else if(this.computeAve().gradeAve < 85){
			//return undefined
			this.studentWithHonors = undefined
		}
		return this
	}



}

// instantiate/create obj fr our Student class
let studentOne = new Student("John", "john@mail.com",[89, 84, 78, 88])
let studentTwo = new Student("Joe", "joe@mail.com",[78, 82, 79, 85])
let studentThree = new Student("Jane", "jane@mail.com",[87, 89, 91, 93])
let studentFour = new Student("Jessie", "jessie@mail.com",[91, 89, 92, 93])


//console.log(studentOne)
// OUTPUT:
// Student {name: 'John', email: 'john@mail.com'}
// email: "john@mail.com"
// name: "John"
// [[Prototype]]: Object

//The name given to a named class expression is local to the class's body. However, it can be 
//accessed via the name property.
// OUTPUT:
// Student.name
// 'Student'

// console.log(studentTwo)
// console.log(studentThree)
// console.log(studentFour)


// after computeAve() is modified; double return not allowed
// studentOne.computeAve()
// Student {name: 'John', email: 'john@mail.com', gradeAve: 84.75, grades: Array(4)}
// email: "john@mail.com"
// gradeAve: 84.75
// grades: (4) [89, 84, 78, 88]
// name: "John"
// [[Prototype]]: Object



